Я пишу очень подробную статью по геометрии на тему треугольники, пожалуйста оптимизируй мета теги для этой страницы

1) Определение. Замечание. Указание. Следствие. Построение.
<dl>
  <dt>Определение.</dt>
  <dd>.</dd>
</dl>
Теорема
<ul id="t3" style="counter-reset: index+3;" class="sample-t anchor">
  <li>
    <dl>
      <dt>Теорема.</dt>
      <dd>Из точки прямой можно провести один и только один перпендикуляр (по одну сторону данной прямой).</dd>
    </dl>
  </li>
</ul>
2) 2 Флекс колонки
<div class="block">
  <div class="block__row">
    <div class="block__column">
      <div class="block__item">
        <ul class="sample">
          <li>$+$ &nbsp; плюс</li>
          <li>$-$ &nbsp; минус</li>
          <li>$>$ &nbsp; больше</li>
          <li>$<$ &nbsp; меньше</li>
          <li>$\times$ &nbsp; умножить на</li>
          <li>$=$ &nbsp; равно</li>
          <li>$\equiv$ &nbsp; равновелик</li>
        </ul>
      </div>
    </div>
    <div class="block__column">
      <div class="block__item">
        <ul class="sample">
          <li>$+$ &nbsp; плюс</li>
          <li>$-$ &nbsp; минус</li>
          <li>$>$ &nbsp; больше</li>
          <li>$<$ &nbsp; меньше</li>
          <li>$\times$ &nbsp; умножить на</li>
          <li>$=$ &nbsp; равно</li>
          <li>$\equiv$ &nbsp; равновелик</li>
        </ul>
      </div>
    </div>
  </div>
</div>
3) 2 Флекс колонки с фото
<div class="block-photo">
  <div class="block-photo__row">
    <div class="block-photo__column">
      <div class="block__item">
        <div class="adapt-photo">
          <img loading="lazy" class="adapt-photo__img" src="image/06/01.png" alt="Геометрический угол, рисунок 1.">
        </div>
      </div>
    </div>
    <div class="block-photo__column">
      <div class="block-photo__item">
        <div class="adapt-photo">
          <img loading="lazy" class="adapt-photo__img" src="image/06/01.png" alt="Геометрический угол, рисунок 1.">
        </div>
      </div>
    </div>
  </div>
</div>
4) теорема и картинка
<div class="block-photo">
              <div class="block-photo__row">
                <div class="block-photo__column">
                  <div class="block__item">
                    <div style="max-width: 500px;">
                      <ul id="t" style="counter-reset: index+3;" class="sample-t anchor">
                        <li>
                          <dl>
                            <dt>Теорема.</dt>
                            <dd>Из точки прямой можно провести один и только один перпендикуляр (по одну сторону данной прямой).</dd>
                          </dl>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                  <div class="block-photo__column">
                    <div class="block-photo__item">
                      <div style="height: 200px;" class="adapt-photo">
                        <img loading="lazy" style="top: 11%" class="adapt-photo__img" src="image/04/06.png" alt="Рисунок к теореме 1.">
                      </div>
                    </div>
                  </div>
              </div>
            </div>
5) номер теоремы в тексте или аксиомы
(см. <a target="_blank" href="06.html">акс. 11.</a>)
(см. <a target="_blank" href="06.html">§ 6.</a>)
(см. <a target="_blank" href="06.html">опред.</a>)
(см. <a target="_blank" href="06.html">след.</a>)

(см. <a href="04.html" target="_blank">теорему</a> <b style="counter-reset: n+4;" class="number-th-text"></b>.)
6) 1 картинка
<center>
<img loading="lazy" style="min-width: 360px; width: 60%; object-fit: cover;" src="image/05/04.png" alt="Геометрический чертеж к теореме 6.">
</center>
7)обратная теорема 
<center><em>Данная теорема является обратною к теореме</em> <a href="#t9"><b style="counter-reset: n+9;" class="number-th-text"></b></a></center>
8)отступ между буквами
<span class="katex" style="letter-spacing: 3px;">$PQ$</span>
9)чтд подсказка
<b class="quest" data-tooltip="Аббревиатура от лат. Quod erat demonstrandum — «Что и требовалось доказать»">Q.E.D.</b>
0) шаблон php отправки почты https://atuin.ru/blog/stilizovannaya-rabochaya-forma-obratnoj-svyazi/


якорь
<div id="p2" style="position:relative; top:-55%;"></div>



(см. следствие <a href="06.html#s9" target="_blank">теоремы</a> <b style="counter-reset: n+15;" class="number-th-text"></b>.)